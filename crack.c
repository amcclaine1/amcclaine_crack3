#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char *password;
    char *hash;
};

int compare(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa -> hash, bb->hash);
}

int fcomp(const void *target, const void *elem)
{
    char *tt = (char *)target;
    struct entry *ee = (struct entry *)elem;
    return strcmp(tt, ee->hash);
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    //Making the structure of passwords and finding the size.
    struct stat info;
    stat(filename, &info);
    int filesize = info.st_size;
    
    char *contents = malloc(filesize + 1);
    
    //Opening and validating passwords.txt file.
    FILE *p = fopen(filename, "r");
    if(!p)
    {
        printf("Cannot open the file for reading.\n");
        exit(1);
    }
    
    //reading p into memalloc contents 1 byte at a time, and closing file P.
    fread(contents, 1, filesize, p);
    fclose(p);
    
    //Adding a null to the end of memalloc.
    contents[filesize] = '\0';
    
    //counting the number of lines
    int passCount = 0;
    for (int i = 0; i < filesize; i++)
    {
        if (contents[i] == '\n') passCount++;
    }
    
    struct entry *password = malloc(passCount * sizeof(struct entry));
    password[0].password = strtok(contents, "\n");
    password[0].hash = md5(password[0].password, strlen(password[0].password));
    
    for(int i = 1; i < passCount; i++)
    {
        password[i].password = strtok(NULL, "\n");
        password[i].hash = md5(password[i].password, strlen(password[i].password));
    }
    
    return password;
}


int main(int argc, char *argv[])
{

    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary("rockyou2000.txt", &dlen) ;
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, 2000, sizeof(struct entry), compare);

    // TODO
    // Open the hash file for reading.
    FILE *hashes = fopen("hashes.txt", "r");
    if(!hashes)
    {
        printf("Could not open hash file for reading.");
    }

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char hashed[1000];
    
    
    while(fgets(hashed, 33, hashes) != NULL)
    {  
        struct entry *found = bsearch(hashed, dict, 2000, sizeof(struct entry), fcomp);
        if(found)
        {
            printf("Password found! Hash: %s || Password: %s\n",found -> hash, found -> password);
        } 
    } 
}
